import React, { Component } from "react";
import http from "axios";
import { Table, Button } from "reactstrap";
import moment from "moment";

class Tabla extends Component {
  constructor(props){
    super(props);
    this.state = {
      consents: []
    };
    this.getData = this.getData.bind(this);
  }
  componentDidMount(){
    this.getData()
  }

  getData() {
    // let config = {
    //   headers: {
    //     'authorization': 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjJCQjE2NDE3MkZBRTU1QkFFMzgyRURDNkNCNDNDMjA4RUM0RDU2QjUiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJLN0ZrRnktdVZicmpndTNHeTBQQ0NPeE5WclUifQ.eyJuYmYiOjE1NTk2MzgyNjEsImV4cCI6MTU1OTY0MTg2MSwiaXNzIjoiaHR0cHM6Ly9pZGVudGlkYWRwcnUuaWJlcmNhamEuZXMvc29wb3J0ZS9wbGF0YWZvcm1hL2lkZW50aWRhZC9hcGkvdjEvIiwiYXVkIjpbImh0dHBzOi8vaWRlbnRpZGFkcHJ1LmliZXJjYWphLmVzL3NvcG9ydGUvcGxhdGFmb3JtYS9pZGVudGlkYWQvYXBpL3YxL3Jlc291cmNlcyIsImliZXJmYWJyaWMuc29wb3J0ZS5hbGZhYmV0aWNvLnBlcnNvbmFzIiwiaWJlcmZhYnJpYy5vbW5pY2FuYWxpZGFkLmNhbmFsZXMuYmFuY2FkaWdpdGFsIl0sImNsaWVudF9pZCI6ImliZXJmYWJyaWMuc29wb3J0ZS5hbGZhYmV0aWNvLnBlcnNvbmFzLnJlbGFjaW9uZXMuc3dhZ2dlciIsInN1YiI6IjQ5OCIsImF1dGhfdGltZSI6MTU1OTYzNTUxNywiaWRwIjoibG9jYWwiLCJuYW1lIjoiMjUwMjUxOTYiLCJsb2dpbiI6IjI1MDI1MTk2IiwibG9naW5fdHlwZSI6InBhcnQiLCJwd2QiOiJleGlzdHMiLCJzY29wZSI6WyJpYmVyZmFicmljLnNvcG9ydGUuYWxmYWJldGljby5wZXJzb25hcy5yZWxhY2lvbmVzIiwiaWJlcmZhYnJpYy5vbW5pY2FuYWxpZGFkLmNhbmFsZXMuYmFuY2FkaWdpdGFsIl0sImFtciI6WyJwd2QiXX0.bq7MrIQaYsLJqdUN0L7LI6glD_JELoRxz_2LGDbtV_i79n-BCT1h62y1565ZXyo19jRa3Hq85aEisWKrPt8EUC8J4Oas_PRjAROgWW_0qrCKGyigTpuJgIHeogBBLd3LSd6xlD9koSAveAFz545xWd876vwmKMGYKeHKm1iFlodjCDEOXnwkxtACdA9l-p-PGbS7LBNT6AloJRItcChUoKcD00BX_C0pSowg2CT51RW2XES-jndaFFkqM2S-hHAJoLeYRL2AjcUnMujsgkjyRgembzFjDeIS_LBFYnWN8L0O603ihcTh75RWTiPdGy0WIs89zjOQe_fDsQsPMrTo5w'
    //   }
    // }
    //http.get("http://localhost:7777/PSD2/portal",config).then((response) => {
    http.get("http://localhost:7777/PSD2/portal")    
    .then(json => {
      console.log(json)
      this.setState({
        consents : json.data.consents
      })
    })
    .catch(e => {
      console.error(e);
    })
  }

  render() {
    let contador = 1;
    let consentsArray = this.state.consents.map(consent => {
      return (
        <tr key={contador}>
          <td>{contador++}</td>
          <td>{consent.consentId}</td>
          <td>{moment(consent.validUntil).format("DD-MM-YYYY")}</td>
          <td className="text-center">{consent.frecuency}</td>
          <td>{consent.psu}</td>
          <td>{consent.tpp}</td>
          <td>{moment(consent.createdDate).format("DD-MM-YYYY")}</td>
          <td>{moment(consent.lastAccess).format("DD-MM HH:mm")}</td>
          <td>
            <Button color="info" size="sm" className="mr-2">
              Opcion 1
            </Button>
            <Button color="info" size="sm" className="mt-2">
              Opcion 2
            </Button>
          </td>
        </tr>
      );
    });

    return (
      <div className="App container">
        <Table>
          <thead>
            <tr>
              <th>ID</th>
              <th>CONSENT ID</th>
              <th>VALIDEZ</th>
              <th>FRECUENCIA</th>
              <th>CLALF</th>
              <th>TPP</th>
              <th>FECHA CREACION</th>
              <th>ULTIMO ACCESO</th>
            </tr>
          </thead>
          <tbody>{consentsArray}</tbody>
        </Table>
      </div>
    );
  }
}
export default Tabla;
